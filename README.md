# Hacky Mono & Hacky Code

This is a personal build of a Hack-based programming font.
It features wider angle brackets and rounder parentheses than the original.
The Zero glyph has a diamond filler.
To distinguish regular and oblique styles a bit better, the regular `i` is using a slab, while the oblique `i` keeps its tail.
Also, oblique `f` gets an underlength.
This creates a nice mixture of oblique and italic style elements.

In addition to that the font is ligaturized.
For that it uses some of Fira Code's ligatures, mainly for Haskell programming.
Hack's original glyphs for `*` and `@` are replaced by the one's from Fira Code.
The variant _Hacky Mono_ comes without programming ligatures.


## Install on Linux

1. Download the latest version.
2. Extract the files from the archive (`.zip`).
3. Copy the font files to either your system font folder (often `/usr/share/fonts/`) or user font folder (often `~/.local/share/fonts/` or `/usr/local/share/fonts`).
4. Copy the font configuration file in `config/fontconfig/` to either the system font configuration folder (often `/etc/fonts/conf.d/`) or the font user folder (often `~/.config/fontconfig/conf.d`)
5. Clear and regenerate your font cache and indexes with the following command:

```
$ fc-cache -f -v
```

You can confirm that the fonts are installed with the following command:

```
$ fc-list | grep "Hacky"
```

Some users may find that font rendering is improved on their distro with [these instructions](https://wiki.manjaro.org/index.php?title=Improve_Font_Rendering).


## This font builds on following projects...

- [Hack](https://github.com/source-foundry/Hack) - basic Hack font types
- [alt-hack](https://github.com/source-foundry/alt-hack) - stylistic alternate glyphs for Hack
- [FiraCode](https://github.com/tonsky/FiraCode) - monospaced font with programming ligatures
- [Ligaturizer](https://github.com/ToxicFrog/Ligaturizer) - script for applying coding ligatures to any font


## License

**Hack** &copy; 2018 Source Foundry Authors. MIT License

**Bitstream Vera Sans Mono** &copy; 2003 Bitstream, Inc. (with Reserved Font Names _Bitstream_ and _Vera_). Bitstream Vera License.

**Fira Code** &copy; 2014 Nikita Prokopov. OFL License

**Ligaturizer** &copy; Ilya Skriblovsky (et al.). GPLv3 License

The Hack font binaries are released under a license that permits unlimited print, desktop, web, and software embedding use for commercial and non-commercial applications.

See [LICENSE.md](https://gitlab.com/wallisers/hacky-code/-/blob/master/LICENSE.md) for the full texts of the licenses.
